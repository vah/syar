$(document).ready(function () {
    //$(".lettering-words").lettering("words"),
    $(".header_next-button").click(function () {
      return (
        $("html, body").animate({ scrollTop: $(".content").offset().top }, 700),
        !1
      );
    }),
    $(".btn_question").click(function () {
      return (
        $("html, body").animate(
          { scrollTop: $("section.contacts").offset().top },
          600
        ),
        !1
      );
    }),
    $(".btn_form__send").click(function () {
        /** Проверка заполненности полей (message + email)*/
        var email   = $('#user_email').val(),
            message = $('#user_message').val(),
            name    = $('#user_name').val();
        if ( email.length && message.length ) {
            feedback.submit();
        }

        return false;
    }),
    $("#video").YTPlayer()
});
