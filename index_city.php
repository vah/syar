<?php
/** Обработка заявки */
if (!empty($_POST['user'])) {
    if (sendMessage($_POST['formData']))
        header('Location:refresh');
    /*else*/
}
/** Отправка сообщения */
function sendMessage($ar)
{
    if (empty($ar['email']) || empty($ar['message']))
        return false;
    /** Отправка сообщения */
    /*mail();*/

    return true;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>krasCITY :: Продающие страницы Красноярска</title>

    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/jquery.flipcountdown.css" rel="stylesheet">
    <link href="/css/styles.css" rel="stylesheet">
    <link href="/style.css" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto&subset=latin,cyrillic' rel='stylesheet' type='text/css'>



<header id="bgVideo">
    <div class="header_top">
        <h2 class="header_logo lettering-words">Качинский Яр</h2>
        <h1 class="header_descr">Коттеджный поселок</h1>
        <div class="header_next-button"><span class="glyphicon glyphicon-chevron-down"></span></div>
    </div>
    <div class="header_bottom">
        <div class="container top-margin-10">
            <div class="col-sm-4 text-right">
                <span class="sign-warning-glyph glyphicon glyphicon-exclamation-sign"></span>
            </div>
            <div class="col-sm-8 text-left font-h2-regular color-light">
                <p>Наш сайт находится в разработке</p>
                <p>Необходимая информация доступна ниже</p>
            </div>
        </div>
    </div>

    <div id="video" class="player" data-property="{videoURL:'9eSncpUEwOk',containment:'#bgVideo', showControls:false, autoPlay:true, loop:true, mute:true, startAt:0, opacity:1, addRaster:true, quality:'default'}">Drokino</div>
</header>

<main class="content">
    <?#==== Кто мы ====?>
    <section class="about container">
        <div class="row">
            <div class="col-xs-12">
                <p class="font-h1 text-uppercase font-header">Кто <span class="color-light-green">мы</span></p>
                <p class="font-h2 text-uppercase top-margin-20">Несколько слов о нашем коттеджном поселке</p>
                <p class="font-regular top-margin-30 font-h3-regular-space">
                    Свой загородный дом в экологически чистом районе, с городским уровнем комфорта в микрорайоне со всей
                    социальной инфраструктурой, в 10 минутах езды от центра Красноярска!
                </p>
            </div>
        </div>
        <div class="row top-padding-20 top-margin-20">
            <div class="col-md-5 text-right">
                <img src="<?=$rel?>images/about_photo.jpg" class="img-responsible" alt="Генеральный директор">
            </div>
            <div class="col-md-7 text-left">
                <p class="font-h2-regular color-light-green">ДМИТРИЙ БУДОВОЙ</p>
                <p class="font-h3-regular">ПРЕДСЕДАТЕЛЬ ПРАВЛЕНИЯ ЖСК "КАЧИНСКИЙ ЯР"</p>
                <p class="font-regular-space top-margin-20">Многим из нас раньше казалось, что это дорого и
                    пока рано об этом думать. Для того, чтобы приблизить мечту о своем доме и превратить
                    в реальность задуманное, создан наш Жилищно-строительный кооператив «Качинский Яр». </p>
                <p class="font-regular-space">Мы предлагаем свой дом с землей по цене 2 млн. рублей.
                    Понимаем, что таких денег на руках у нас нет, но есть постоянный заработок в семье,
                    и платить ежемесячно сумму от 17.000 руб. для нас посильно. Предлагаем всю сумму зафиксировать
                    при вступлении в кооператив и разбить на равные платежи на 10 лет. И это без участия банков,
                    а значит без переплат, без залогов и поручителей!</p>
                <a class="btn btn-light-green btn_question" href="#" role="button">ЗАДАТЬ ВОПРОС</a>
            </div>
        </div>
    </section>

    <?#==== Мы в цифрах ====?>
    <section class="tech">
        <div class="container">
            <div class="row">
                <p class="font-h1 font-header color-light">МЫ В <span class="color-light-green">ЦИФРАХ</span></p>
            </div>
            <div class="row top-margin-20">
                <div class="col-lg-3 col-md-6">
                    <div class="circle-light"><span>6</span></div>
                    <div class="color-light-green font-header">км</div>
                    <div class="color-light top-margin-20 font-h3-regular-space">Участки находятся в непосредственной близости от Красноярска. Ближайший &mdash; в 6 км по дороге</div>

                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="circle-light"><span>2</span></div>
                    <div class="color-light-green font-header">млн</div>
                    <div class="color-light top-margin-20 font-h3-regular-space">2 миллиона рублей стоимость индивидуального дома с участком "под ключ"</div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="circle-light"><span>10</span></div>
                    <div class="color-light-green font-header">лет</div>
                    <div class="color-light top-margin-20 font-h3-regular-space">Рассрочка платежа без переплат без участия банка</div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="circle-light"><span>17</span></div>
                    <div class="color-light-green font-header">тыс. руб</div>
                    <div class="color-light top-margin-20 font-h3-regular-space">17600 руб. &mdash; ежемесячный платеж</div>
                </div>
            </div>
        </div>
    </section>

    <?#==== Наши планы ====?>
    <section class="plans container">
        <div class="row">
            <div class="col-xs-12">
                <p class="font-h1 font-header">НАШИ <span class="color-light-green">ПЛАНЫ</span></p>
                <p class="font-h2 top-margin-20">ПЛАНЫ РАЗВИТИЯ КООПЕРАТИВА НА БЛИЖАЙШЕЕ БУДУЩЕЕ</p>
                <p class="font-regular top-margin-30">
                    В виду того, что документы по изначально предлагаемому участку 50 Га на горе д.Дрокино на данный момент находятся в разработке.
                    Хотим предложить Вам на рассмотрение  другие участки ИЖС на тех же условиях.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="experience-item experience-item-with-appearance">
            <div class="experience-item-wrapper">
                <div class="experience-info-holder">
                    <div class="experience-info">
                        <div class="experience-date">
                            <div class="experience-date-right">
                                <div class="experience-date-end-month">декабрь</div>
                                <div class="experience-date-end-year">2017</div>
                            </div>
                            <div class="experience-date-left">
                                <div class="experience-date-start-month"></div>
                                <div class="experience-date-start-year">2016</div>
                            </div>
                        </div>
                        <div class="experience-company">Нулевой этап</div>
                        <div class="experience-position">Работа с документами</div>
                    </div>
                </div>
                <div class="experience-desc-holder">
                    <div class="experience-desc">
                        <h5>
                            <a href="#" title="Проведение проектных работ. Перевод земли в категорию по ИЖС">
                                ПРОВЕДЕНИЕ ПРОЕКТНЫХ РАБОТ. ПЕРЕВОД ЗЕМЛИ В КАТЕГОРИЮ ПО ИЖС <br>
                                (на участке проектируемого жилого мкрн. на 3.5 тыс. жителей)
                            </a>
                        </h5>
                        <p class="font-regular-space">Проведение геодезических работ. Разработка жилой части проекта.
                            Разработка проекта сетей. Получение тех. условий. Перевод земли из категории земель с/х-назначения
                            в земли поселения, изменение проекта расширения села Дрокино.
                            Работа по переводу земли в категорию ИЖС.
                        </p>
                    </div>
                </div>
            </div>
            </div>

            <div class="experience-item experience-item-with-appearance">
            <div class="experience-item-wrapper">
                <div class="experience-info-holder">
                    <div class="experience-info">
                        <div class="experience-date">
                            <div class="experience-date-right">
                                <div class="experience-date-end-month">сентябрь</div>
                                <div class="experience-date-end-year">2017</div>
                            </div>
                            <div class="experience-date-left">
                                <div class="experience-date-start-month">апрель</div>
                                <div class="experience-date-start-year">2017</div>
                            </div>
                        </div>
                        <div class="experience-company">Первый этап</div>
                        <div class="experience-position">Начало строительства</div>
                    </div>
                </div>
                <div class="experience-desc-holder">
                    <div class="experience-desc">
                        <h5>
                            <a href="#" title="Donec sit amet eros. Lorem ipsum dolor sit amet">
                                Строительство объектов первой очереди
                            </a>
                        </h5>
                        <p class="font-regular-space">
                            Запуск производства стройматериалов необходимых для строительства наших домов.
                            Начало строительства и ввод в эксплуатацию домов на предлагаемых участках.
                            Начало строительства домов 1-й очереди проектируемого микрорайона (по готовности документов на землю).
                            Подключение к электросетям по постоянной схеме. Организация подачи питьевой воды в дома.
                        </p>
                    </div>
                </div>
            </div>
            </div>

            <div class="experience-item experience-item-with-appearance">
            <div class="experience-item-wrapper">
                <div class="experience-info-holder">
                    <div class="experience-info">
                        <div class="experience-date">
                            <div class="experience-date-right">
                                <div class="experience-date-end-month">Апрель</div>
                                <div class="experience-date-end-year">2018</div>
                            </div>
                            <div class="experience-date-left">
                                <div class="experience-date-start-month">Октябрь</div>
                                <div class="experience-date-start-year">2017</div>
                            </div>
                        </div>
                        <div class="experience-company">Второй этап</div>
                        <div class="experience-position">Заселение</div>
                    </div>
                </div>
                <div class="experience-desc-holder">
                    <div class="experience-desc">
                        <h5>
                            <a href="#" title="">
                                Сдача объектов первой очереди. Строительство объектов второй очереди
                            </a>
                        </h5>
                        <p class="font-regular-space">Строительство газовой станции. Строительство таунхаусов.
                        </p>
                    </div>
                </div>
            </div>
            </div>

            <div class="experience-item experience-item-with-appearance">
                <div class="experience-item-wrapper">
                    <div class="experience-info-holder">
                        <div class="experience-info">
                            <div class="experience-date">
                                <div class="experience-date-right">
                                    <div class="experience-date-end-month">Декабрь</div>
                                    <div class="experience-date-end-year">2018</div>
                                </div>
                                <div class="experience-date-left">
                                    <div class="experience-date-start-month">Декабрь</div>
                                    <div class="experience-date-start-year">2017</div>
                                </div>
                            </div>
                            <div class="experience-company">Окончание</div>
                            <div class="experience-position">&nbsp;</div>
                        </div>
                    </div>
                    <div class="experience-desc-holder">
                        <div class="experience-desc">
                            <h5>
                                <a href="#" title="Финальный этап">
                                    Финальный этап
                                </a>
                            </h5>
                            <p class="font-regular-space">Сдача многоквартирных домов, объектов социальной инфраструктуры
                                (садик, школа, поликлиника, аптеки и т.д.). Окончание всех строительных работ.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?#==== Карта ====?>
    <section id="map" class="container"></section>

    <?#==== Контакты ====?>
    <section class="contacts">
        <div class="container">
            <div class="row">
                <p class="font-h1 font-header color-light">КАК С НАМИ <span class="color-light-green">СВЯЗАТЬСЯ</span></p>
            </div>
            <div class="col-md-5 top-margin-30">
                <div class="address-group">
                    <div class="address-glyph">
                        <span class="glyphicon glyphicon-home"></span>
                    </div>
                    <div class="address-text">Красноярск, <br>Вавилова, 1, <br>ст. 1-304</div>
                </div>
                <div class="address-group top-padding-20">
                    <div class="address-glyph">
                        <span class="glyphicon glyphicon-phone-alt"></span>
                    </div>
                    <div class="address-text top-margin-10">(391) 209 69 90 <br>(391) 209 69 91</div>
                </div>
                <div class="address-group top-padding-20">
                    <div class="address-glyph">
                        <span class="glyphicon glyphicon-envelope"></span>
                    </div>
                    <div class="address-text top-margin-20">mail@kachyar.ru</div>
                </div>
            </div>
            <div class="col-md-7 top-margin-20">
                <form class="form-inline" role="form" id="_form5" action="/" method="post">
                    <div class="form-content">
                        <input type="text" id="user_name" name="user[name]" value="" size="40"
                               class="contact-form-control contact-text <?#contact-validates-as-required?>"
                               aria-required="true" aria-invalid="false" placeholder="Имя:">
                        <input type="text" id="user_email" name="user[email]" value="" size="40"
                               class="contact-form-control contact-text"
                               aria-required="true" aria-invalid="false" placeholder="E-mail:">
                        <textarea id="user_message" name="user[message]" cols="40" rows="10"
                                  class="contact-form-control contact-textarea"
                                  aria-invalid="false" placeholder="Ваш вопрос:"></textarea>
                    </div>
                    <div class="form-content text-left">
                        <a class="btn btn-light-green text-left btn_form__send" href="#" role="button">ОТПРАВИТЬ</a>
                    </div>
                </form>
            </div>
        </div>
    </section>

</main>


<footer>Footer</footer>

    </div><!-- page -->

    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.flipcountdown.js"></script>
    <script src="/scripts.js"></script>

    </body>
</html>
